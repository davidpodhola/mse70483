﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class Employee
    {
        protected string EmployeeType
        {
            get;
            set;
        }

        private void Do1()
        {
            EmployeeType = "aaa";
            Console.WriteLine(EmployeeType);
        }
    }

    public class A : Employee
    {
        private void Do1()
        {
            EmployeeType = "aaa";
            Console.WriteLine(EmployeeType);
        }
    }

    public class Logger
    {
        [Conditional("DEBUG")]
        public static void LogLine()
        {
        }
    }

    public class Communicate {
        private async void GetData( WebResponse response ) 
        {
            var streamReader = new StreamReader( response.GetResponseStream() );
            string text = await streamReader.ReadLineAsync();
        }
    }

    public static class ExtensionMethids
    {
        public static bool IsEmail(this String str)
        {
            var regex = new Regex(@"", RegexOptions.Compiled);
            return regex.IsMatch(str);
        }
    }

    public class Book {
        private string name;

        public Book(string name)
        {
            this.name = name;
        }
    }

    public delegate void AddBookCallback( int i );
    public class BookTracker
    {
        List<Book> books = new List<Book>();
        public void AddBook(string name, AddBookCallback callback)
        {
            books.Add(new Book(name));
            callback(books.Count);
        }
    }

    public class Runner
    {
        BookTracker tracker = new BookTracker();
        public void Add(string name)
        {
            tracker.AddBook(name, delegate (int i) {
                Console.WriteLine(i);
            });
        }
    }

    public class Lease
    {
        public event MaximumTermReachedHandler OnMaximumTermReached;

        private int _term;
        private const int MaximumTerm = 5;
        private const decimal Rate = 0.034m;

        public int Term
        {
            get
            {
                return _term;
            }
            set
            {
                if (value <= MaximumTerm)
                {
                    _term = value;
                }
                else
                {
                    if (OnMaximumTermReached != null)
                    {
                        OnMaximumTermReached(this, new EventArgs());
                    }

                }
            }
        }
    }

    public delegate void MaximumTermReachedHandler( object source, EventArgs e ); 

    class Program
    {
        static void Main(string[] args)
        {
            /*
            var sb = new StringBuilder();
            sb.Append("First line");
            sb.AppendLine();
            sb.Append("Second line");

            Console.WriteLine(sb.ToString());
            Console.ReadKey();
            

            
            CounterCreationDataCollection ccdc = new CounterCreationDataCollection();
            PerformanceCounterCategory pcc = PerformanceCounterCategory.Create("CNL", "CH", ccdc); 
            CounterCreationData ccd = new CounterCreationData();
            ccdc.Add(ccd);
            

            Lease l = new Lease();
            l.OnMaximumTermReached += l_OnMaximumTermReached;
            l.Term = 10;
            Console.ReadKey();
             */

            /*
            Runner r = new Runner();
            r.Add("test");
            Console.ReadKey();
            */

            /*
            decimal[] loadAmounts = { 303m, 1000m, 85579m, 501.51m, 603m, 1200m, 400m, 22m };
            IEnumerable<decimal> loanQuery =
                from amount in loadAmounts
                where amount % 2 == 0
                orderby amount ascending
                select amount;

            foreach (decimal d in loanQuery) Console.WriteLine(d);
            Console.ReadKey();
             */

            //Task.ContinueWith();

            /*
            String s = "a@b.c";
            Console.WriteLine( s.IsEmail() );
             */

            //[assembly: AssemblyDelaySign( true )]
            //[assembly: AssemblyKeyFile( "C:\\test.key" ) ]

            // http://msdn.microsoft.com/en-US/library/yf1d93sz(v=vs.80).aspx
            // gacutil + Installer

            /*
            var client = new WebClient();
            var nvc = new NameValueCollection() { { "a", 1.ToString() }, {"b", 2.ToString() } };
            client.UploadValuesTaskAsync("", nvc);
             */

            // HMACSHA256 

            /*
            Employee e = new Employee();
            //e.EmployeeType = "aaa";
             */

            //PerformanceCounterType.SampleBase

            //GC.KeepAlive();
            //GC.SuppressFinalize();
        }

        static void l_OnMaximumTermReached(object source, EventArgs e)
        {
            Console.WriteLine("caution!!");
        }
    }
}
